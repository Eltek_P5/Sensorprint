/*
 * FILNAVN :    Sensorprint.ino 
 *
 * Beskrivelse :
 *      Videresender værdier fra fjernbetjening, og styrer led'er.
 * 
 * Forfatter : 
 *      Gustav, Julian, Laurits, Victor
 */
#include <Adafruit_NeoPixel.h>

//Debug pin opsætning
int debugPin = 18;
bool debugState = true;

//Lys styrke, 255 er max
const int bright = 127;

//Antal pixels
const int numPixels = 12;

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(numPixels, 14, NEO_GRB + NEO_KHZ800);

//Pins til echo afstandmålere
int triggerf = 20;
int triggert = 22;
int echof = 19;
int echot = 21;

void setup()
{
    //Sæt pins op
    pinMode(debugPin, OUTPUT);
    pinMode(triggert, OUTPUT);
    pinMode(triggerf, OUTPUT);
    pinMode(echot, INPUT);
    pinMode(echof, INPUT);

    //Start forskellige ting
    Serial.begin(115200);
    Serial1.begin(115200);

    //Start leds som hvid
    pixels.begin();

    setAllPixels(255,255,255);
    pixels.show();

    digitalWrite(debugPin, HIGH);
    
}

//Vil blinke led'en
void burst(){
    debugState = !debugState;
    digitalWrite(debugPin, debugState);
}

//Læser afstanden og giver i helt tal, cm
int readDistance(int tP, int eP){

    //Pulse pinnen i 10 microsekunder
    digitalWrite(tP, LOW);
    delayMicroseconds(2);
    digitalWrite(tP, HIGH);
    delayMicroseconds(10);
    digitalWrite(tP, LOW);

    //Mål tiden som echopinnen får
    long time = pulseIn(eP, HIGH, 3000);

    //Regn afstanden ud
    float afstand = time / 57.0;

    return afstand;
}

//Sætter alle pixels på en gang
void setAllPixels(int r, int g, int b)
{
    for(int i = 0; i < numPixels; i++){
        pixels.setPixelColor(i, r, g, b, bright); 
    }
}


char handleInput(char c)
{
    //Hvis den stopper, bliver den hvis, ellers grøn
    if(c == 's'){
        //Hvid
        setAllPixels(255, 255, 255);    
    }else if(c == 'f' || c == 't'){
        setAllPixels(0, 255, 0);
    }  
    
    //Læs afstanden på hver side
    int afstandf = readDistance(triggerf, echof);
    int afstandt = readDistance(triggert, echot);

    //Hvis den er tæt på noget kører den modsat vej, og lyser rød
    if(afstandf > 0 && afstandf < 15){
        setAllPixels(255, 0, 0);
        return 't';
    }
    if(afstandt > 0 && afstandt < 15){
        setAllPixels(255, 0, 0);
        return 'f';
    }
    return c;
}

void loop()
{
    //Hvis der er serial bliver denne kørt
    if(Serial.available()){
        char c = Serial.read();
        Serial1.write(handleInput(c));
        pixels.show();
    }
}
